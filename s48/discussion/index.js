// console.log("Hello, World!");

// Mock database
let posts = [];

// Post ID
let count = 1;

// Add post 
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	// Prevents the page from loading
	e.preventDefault();

	posts.push({
		id: count,
		// "title" and "body" values will come from the input elements in the form
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	// count will increment everytime a new post is posted
	count++;

	alert("Successfully added!");
	showPosts(posts);
})

// Show posts
const showPosts = (posts) => {

	// Creates a variable that will contain all the posts
	let postEntries = "";

	// forEach() - to loop over each post in our posts mock database
	posts.forEach((post) => {
		console.log(post);

		// We can assign HTML elements in JS variables
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});

	// To display the posts in our HTML document
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit post
// This will trigger an event that will update a certain post upon clicking thSe edit button

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;


};

// Update post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();

	// Loops through every post in the "posts" array starting from the first element/index 0
	for(let i = 0; i < posts.length; i++) {

		if(posts[i].id == document.querySelector("#txt-edit-id").value) {
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			showPosts(posts);
			alert("Successfully updated!");
			break;
		}
	}
});

// [ACTIVITY] Delete Post

const deletePost = (id) => {
  // Find the index of the post with the given id in the posts array
  const index = posts.findIndex((post) => post.id == id);

  if (index !== -1) {
    // Remove the post from the posts array using splice()
    posts.splice(index, 1);

    // Remove the corresponding element from the DOM
    const postElement = document.querySelector(`#post-${id}`);
    if (postElement) {
      postElement.remove();
    }

    alert("Post deleted successfully!");
  }
};
